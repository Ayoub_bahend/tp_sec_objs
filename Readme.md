# TP_SEC_OBJS

Ce script Python utilise la bibliothèque Bleak pour interagir avec des périphériques Bluetooth Low Energy (BLE). Il permet de se connecter à un périphérique, de démarrer une notification sur une caractéristique spécifique, d'écrire une valeur à une autre caractéristique, de lire les caractéristiques disponibles et de récupérer un fichier à partir du périphérique.

## Installation

- Assurez-vous d'avoir Python installé sur votre système.

- Installez les dépendances en exécutant la commande suivante :
```
pip install bleak
```

## Utilisation

- Assurez-vous que votre adaptateur Bluetooth est activé et à portée.
- Exécutez le script en utilisant la commande suivante :

```
python script.py
```

## Fonctionnalités

- Notification Handler : Lorsqu'une notification est reçue, les données sont affichées en format hexadécimal.
- Démarrage Notification : Permet de démarrer une notification sur une caractéristique spécifique et d'écrire une valeur à une autre caractéristique.
- Lecture des Caractéristiques : Affiche les caractéristiques disponibles sur le périphérique ainsi que les valeurs associées.
- Récupération de Fichier : Permet de récupérer un fichier à partir d'un périphérique BLE.

## Configuration

- Adresse MAC du Périphérique : Remplacez FE:F7:A2:37:6E:28 par l'adresse MAC de votre périphérique BLE.
- UUID des Caractéristiques : Remplacez les UUID des caractéristiques selon les spécifications de votre périphérique.
