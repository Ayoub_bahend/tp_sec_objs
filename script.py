# Importation des modules nécessaires
import asyncio
from bleak import BleakClient, BleakError

# Définition du gestionnaire de notification qui sera appelé lorsqu'une notification est reçue
async def notification_handler(sender, data):
    # Affiche les données reçues en les convertissant en hexadécimal et en les séparant par des virgules
    print("Données reçues :", ', '.join(f'{x:02x}' for x in data))

# Fonction pour démarrer la notification et écrire une valeur à une caractéristique spécifique
async def start_notification(mac_address, characteristic_uuid, value_to_write):
    try:
        # Connexion au périphérique avec lequel communiquer
        async with BleakClient(mac_address, timeout=20) as client:
            # Démarrage de la notification pour la caractéristique spécifiée
            await client.start_notify(characteristic_uuid, notification_handler)
            print("Abonnement aux notifications a commencé")
            # Écriture d'une valeur à la caractéristique spécifiée
            await client.write_gatt_char(characteristic_uuid, value_to_write)
            print("Valeur écrite avec succès !")
            # Attente pendant un certain temps pour permettre la réception des notifications
            await asyncio.sleep(10)
            print("Attente terminée")
    except BleakError as e:
        # Affiche une erreur en cas de problème lors de la connexion ou de l'écriture
        print(f"Erreur lors de la connexion à {mac_address}: {e}")

# Fonction pour lire les caractéristiques d'un périphérique BLE
async def read_characteristics(address):
    try:
        # Connexion au périphérique
        async with BleakClient(address, timeout=2000.0) as client:
            # Récupération des services disponibles sur le périphérique
            services = await client.get_services()

            # Parcours de tous les services
            for service in services:
                print(f"Service UUID: {service.uuid}")
                # Parcours de toutes les caractéristiques du service actuel
                for characteristic in service.characteristics:
                    print(f"Characteristic UUID: {characteristic.uuid}")
                    try:
                        # Lecture de la valeur de la caractéristique actuelle
                        data = await client.read_gatt_char(characteristic)
                        print("Read data:", data)                       
                    except BleakError as e:
                        # Affichage d'une erreur en cas de problème lors de la lecture
                        print(f"Error reading characteristic {characteristic.uuid}: {e}")

    except BleakError as e:
        # Affiche une erreur en cas de problème lors de la connexion au périphérique
        print(f"Error occurred while connecting to {address}: {e}")

# Fonction pour récupérer un fichier d'un périphérique
async def recuperer_fichier(mac_address, Write_uuid):
    try:
        # Connexion au périphérique BLE
        async with BleakClient(mac_address, timeout=20) as client:
            # UUID de la caractéristique à laquelle s'abonner pour récupérer le fichier
            Write_uuid = "1b0d1304-a720-f7e9-46b6-31b601c4fca1"
            # Démarrage de la notification pour la caractéristique spécifiée
            await client.start_notify(Write_uuid, notification_handler)
            print("Abonnement aux notifications a commencé")
            # Valeur à écrire à la caractéristique pour demander le fichier
            value_request = b'T13\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x27\x0f'   
            await client.write_gatt_char(Write_uuid, value_request)
            # Attente pendant 10s pour permettre la réception des notifications
            await asyncio.sleep(10)
            print("Attente terminée")
    except BleakError as e:
        # Affiche une erreur en cas de problème lors de la connexion ou de l'écriture
        print(f"Erreur lors de l'écriture à {mac_address}: {e}")

async def main():
    try:
        # Adresse du périphérique
        device_address = "FE:F7:A2:37:6E:28"
        # UUID de la caractéristique et valeur à écrire
        characteristic_uuid = "1b0d1302-a720-f7e9-46b6-31b601c4fca1"
        value_to_write = bytearray(b'\x05\x00')
        
        # Appel des fonctions pour démarrer la notification et lire les caractéristiques
        await start_notification(device_address, characteristic_uuid, value_to_write)
        await read_characteristics(device_address)
        # Appel de la fonction pour récupérer le fichier
        Write_uuid = "1b0d1304-a720-f7e9-46b6-31b601c4fca1"
        await recuperer_fichier(device_address, Write_uuid)

    except BleakError as e:
        # Affiche une erreur en cas de problème lors de la connexion ou de l'interaction avec le périphérique BLE
        print(f"Error occurred while scanning for devices: {e}")

asyncio.run(main())
